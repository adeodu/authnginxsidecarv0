var express = require('express');
var path = require('path');
var app = express();

var bodyParser = require('body-parser');

var port = process.env.PORT || 8086;
app.use(bodyParser.urlencoded({extended: true})); // so we can get info from POST and/or URL parameters
app.use(bodyParser.json());

app.use(express.static(__dirname + '/app'));

// get an instance of the router for API routes
var authRoute = express.Router();
app.use(express.Router());

app.use('/auth', authRoute);

var authApi = require('../app/js/authApi')(app, port, authRoute);

app.use('*', function(req, res) {
	res.writeHead(404, {'Content-Type': 'text/html'});
	res.write('<div style="margin-left:20;margin-top:30px;"><h2>Authorization failed!</h2></div></div>');
	res.end();
	res.send();
});

app.listen(port, '', function() {
	// console.log('rootPath is:' + rootPath);
	console.log('Express started on ' + port + ';. press Ctrl-C to terminate.');
});
