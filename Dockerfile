FROM mhart/alpine-node:6.2.0

RUN mkdir -p /usr/src/app  
WORKDIR /usr/src/app  
COPY . /usr/src/app

EXPOSE 8086
RUN npm install

CMD node scripts/web-server.js

# You can then build and run the Docker image:
# $ docker build -t auth-api-v0 .
# $ docker run -it -p 86:8086 auth-api-v0
